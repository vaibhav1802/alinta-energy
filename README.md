ALINTA CODE CHALLENGE:
##PROJECT DEPENDENCIES:
1. React Router: For routing
2. Jest and Enzyme: For unit test. Enzyme: For React Component. Jest: For javascript functions
3. Proptypes for prop typechecking
4. Node sass for sass style compiler. I am a fan of sass/less than React inline style. But for the demo purpose I have styled couple of React components using React inline style.
5. Webpack: For webpack dev server and bundler
6. React datepicker: for getting user input for birth date
7. Redux: For managing the state across the app. Not necessary in such a small app where state can be managed at the parent level and info can be passed to child components either by prop drilling, or ContextAPI or Event bus like FBEmitter. I have used redux because of the project requirement doc. 

#IMPROVEMENTS:
With more time in hand, I would do:
1. Better styling.
2. Write more unit tests for components.
3. I don't have the experience of working with TypeScript. I would definitely learn and understand the proper use case of Typescript for implementation.

##REQUIREMENTS
Have the candidate build a simple interface (API or UI) that allows:
  Adding customers.
  First name, last name and date of birth fields.
Editing customers.
Deleting customers.
Searching for a customer by partial name match (first or last name).



## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:8080] to view it in the browser.

### `npm test`
Launches the test runner.

