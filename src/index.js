import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/index.scss';
import App from './app';
import store from './state-management/store';
import {Provider} from 'react-redux';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
document.getElementById('root'));
module.hot.accept();
