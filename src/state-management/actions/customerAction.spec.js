import { 
  ADD_CUSTOMER, 
  DELETE_CUSTOMER, 
  EDIT_CUSTOMER 
} from '../constants';

import * as actions from './customerAction';

describe('Customer Actions', () => {
  it('should create an action to add the customer', () => {
    const data = {
      customerId: '_tsgtahk',
      firstName: 'Mike',
      lastName: 'Tyson',
      dob: new Date()
    };
    const expectedAction = {
      type: ADD_CUSTOMER,
      payload: data
    };
    expect(actions.addCustomer(data)).toEqual(expectedAction);
  });

  it('should create an action to delete the customer', () => {
    const data = {
      customerId: '_tsgtahk'
    };
    const expectedAction = {
      type: DELETE_CUSTOMER,
      payload: data
    };
    expect(actions.deleteCustomer(data)).toEqual(expectedAction);
  });

  it('should create an action to edit the customer', () => {
    const data = {
      customerId: '_tsgtahk',
      firstName: 'Mike',
      lastName: 'Tyson',
      dob: new Date()
    };
    const expectedAction = {
      type: EDIT_CUSTOMER,
      payload: data
    };
    expect(actions.editCustomer(data)).toEqual(expectedAction);
  });
});