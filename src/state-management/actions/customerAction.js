import { 
  ADD_CUSTOMER, 
  DELETE_CUSTOMER, 
  EDIT_CUSTOMER 
} from '../constants';

export const addCustomer = function(data) {
  return {
    type: ADD_CUSTOMER,
    payload: data
  }
}

export const deleteCustomer = function(customerId) {
  return {
    type: DELETE_CUSTOMER,
    payload: customerId
  }
}

export const editCustomer = function(customerInfo) {
  return {
    type: EDIT_CUSTOMER,
    payload: customerInfo
  }
}