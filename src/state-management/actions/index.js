import { bindActionCreators } from 'redux';
import * as customerObj from './customerAction';
import store from '../store';

const actions = bindActionCreators({
 ...customerObj
}, store.dispatch);

export default actions;