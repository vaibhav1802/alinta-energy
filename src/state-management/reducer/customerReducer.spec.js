import { 
  ADD_CUSTOMER, 
  DELETE_CUSTOMER
} from '../constants';
import initialState from '../state';
import customerReducer from './';

describe('Customer reducer', () => {
  it('should return the initial state', () => {
    expect(customerReducer(undefined, {})).toEqual(initialState);
  });

  it('should handle ADD_CUSTOMER action', () => {
    const data = {
      customerId: '_tsgtahk',
      firstName: 'Mike',
      lastName: 'Tyson',
      dob: new Date()
    };
    const action = {
      type: ADD_CUSTOMER,
      payload: data
    };
    const expectedState = {
      ...initialState,
      customerCollection: [...initialState.customerCollection, data]
    };
    expect(customerReducer(initialState, action)).toEqual(expectedState);
  });

  it('should handle DELETE_CUSTOMER action', () => {
    const data = {
      customerId: '_tsgtahk'
    };
    const action = {
      type: DELETE_CUSTOMER,
      payload: data
    };
    const expectedState = {
      ...initialState,
      customerCollection: [...initialState.customerCollection.filter(customer =>
        customer.customerId !== action.payload
      )]
    };
    expect(customerReducer(initialState, action)).toEqual(expectedState);
  });
});