import { ADD_CUSTOMER, DELETE_CUSTOMER, EDIT_CUSTOMER } from '../constants';
import customerCollection from '../state';

export default function (state = customerCollection, action) {
  switch (action.type) {
    case ADD_CUSTOMER:
      return {
        ...state,
        customerCollection: [...state.customerCollection, action.payload]
      }
    case DELETE_CUSTOMER:
      return {
        ...state,
        customerCollection: [...state.customerCollection.filter(customer =>
          customer.customerId !== action.payload
        )]
      }
    case EDIT_CUSTOMER:
      return {
        ...state,
        customerCollection: [...state.customerCollection.map(customer =>
          customer.customerId === action.payload.customerId ?
          {...customer, 
            customerId: action.payload.customerId,
            firstName: action.payload.firstName,
            lastName: action.payload.lastName,
            dob: action.payload.dob
          } : customer
        )]
      }
    default: 
      return state;
  }
}
