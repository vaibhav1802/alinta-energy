import React from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';
import Home from '../pages/Home';
import EditCustomer from '../pages/EditCustomer';

const applicationRoutes = (
  <Switch>
    <Route name="home" path="/home" component={Home} />
    <Route name="editCustomer" path="/edit/:id" component={EditCustomer} />
    <Redirect exact from="/" to="home" />
  </Switch>
);

export default applicationRoutes;
