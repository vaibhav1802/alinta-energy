// This container component holds all the major component to provide different functionalities
// It connects to redux to get the added customers
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CustomerAddForm from '../../components/CustomerAddForm';
import CustomerListDisplay from '../../components/CustomerListDisplay';
import TextInput from '../../components/TextInput';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filteredCustomerList: [],
      isSearched: false
    };
    this.onSearch = this.onSearch.bind(this);
  }

  componentDidMount() {
    this.setState({
      filteredCustomerList: this.props.customerCollection
    });
  }

  onSearch(e) {
    const searchTextLc = e.target.value.toLocaleLowerCase();
    let filteredCustomers = [];
    if (searchTextLc !== '') {
      const customerList = this.props.customerCollection;
      filteredCustomers = customerList.filter((customer) => {
        const customerFullNameLc = (`${customer.firstName} ${customer.lastName}`).toLocaleLowerCase();
        return customerFullNameLc.includes(searchTextLc)
      });
      this.setState({
        isSearched: true,
        filteredCustomerList: filteredCustomers
      });
    } else {
      this.setState({
        isSearched: false,
        filteredCustomerList: []
      });
    }
  }

  render() {
    const {filteredCustomerList} = this.state;
    const {customerCollection} = this.props;
    return(
      <div id="home">
        <h1 className="title">Customer Management</h1>
        <div className="search-bar-wrapper">
          <TextInput 
            label="Search customer"
            onInputType={this.onSearch}
            placeholder="Search by customer name"
          />
        </div>
        <div className="search-result-wrapper">
          {
            this.state.isSearched &&
            <React.Fragment>
              <h3 className="search-result-title">Search Result: <span>{filteredCustomerList.length} match found</span></h3>
              <CustomerListDisplay customerList={filteredCustomerList}/> 
            </React.Fragment>
          }
        </div>
        <div className="parent-wrapper-flex">
          <div className="customer-addition-wrapper">
            <h3 className="customer-add-title">Add Customer</h3>
            <CustomerAddForm />
          </div>
          <div className="customer-display-wrapper">
            <h3 className="customer-display-title">Added Customers:</h3>
            <CustomerListDisplay customerList={customerCollection}/>
          </div>
        </div>
      </div>
    )
  }
};

Home.propTypes = {
  customerCollection: PropTypes.array
};

const mapStateToProps = (state) => {
  return {
    customerCollection: state.customerCollection
  };
}

export default connect(mapStateToProps)(Home);