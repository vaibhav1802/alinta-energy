// This component provides the functionality to edit customer info
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Form from '../../components/Form';
import actions from '../../state-management/actions';

class EditCustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSaved: false
    }
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleFormSubmit(formState) {
    event.preventDefault();
    const customerId = this.props.customerInfo.customerId;
    const formStateUpdated = Object.assign({}, formState, {customerId: customerId});
    actions.editCustomer(formStateUpdated);
    this.setState({
      isSaved: true
    });
  }

  render() {
    const {customerInfo} = this.props; 
    const {isSaved} = this.state;
    return(
      <div id="edit">
        <h1 className="title">Edit customer</h1>
        <Form 
          firstName={customerInfo.firstName}
          lastName={customerInfo.lastName}
          date={customerInfo.dob}
          inputBtnLabel="SAVE INFO"
          handleFormSubmit={this.handleFormSubmit}
        />
        {!isSaved &&
          <a className="btn cancel" href="#home">Cancel</a>
        }
        {
          isSaved &&
          <React.Fragment>
            <a className="btn home" href="#home">Back Home</a>
            <p>Your details are saved!</p>
          </React.Fragment>
        }
      </div>
    );
  }
};


const mapStateToProps = (state, params) => {
  const customerId = params.match.params.id;
  const customerObj = state.customerCollection.find(customer => customer.customerId === customerId);
  
  return {
    customerInfo: customerObj
  };
}

export default connect(mapStateToProps)(EditCustomer);