import React, { Component } from 'react';
import Form from '../Form';
import actions from '../../state-management/actions';
import generateUniqueString from '../../utilities/generateUniqueString';

export default class CustomerAddForm extends Component {
  constructor(props) {
    super(props);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  handleFormSubmit(formState) {
    event.preventDefault();
    const firstNameVal = formState.firstName;
    const lastNameVal = formState.lastName;   
    if (firstNameVal !== '' && lastNameVal !== '') {
      const customerId = generateUniqueString();
      this.addCustomer(formState, customerId)
    }
  }

  addCustomer(formState, customerId) {   
    const customer = {
      firstName: formState.firstName,
      lastName: formState.lastName,
      customerId: customerId,
      dob: formState.dob
    };
    actions.addCustomer(customer);
  }

  render() { 
    return(
      <Form 
        firstName=''
        lastName=''
        date={new Date()}
        inputBtnLabel="ADD CUSTOMER"
        handleFormSubmit={this.handleFormSubmit}
      />
    );
  }
};