// This is a resuable form component which is getting used for customer addition and
// customer edit functionality.
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextInput from '../TextInput';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: props.firstName,
      lastName: props.lastName,
      dob: props.date
    };
    this.onFirstNameInputType = this.onFirstNameInputType.bind(this);
    this.onLastNameInputType = this.onLastNameInputType.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
  }

  onFirstNameInputType(e) {
    this.setState({
      firstName: e.target.value
    });
  }

  onLastNameInputType(e) {
    this.setState({
      lastName: e.target.value
    });
  }

  onDateChange(date) {
    this.setState({
      dob: date
    });
  }

  render() {
    const {firstName, lastName, dob} = this.state; 
    const {inputBtnLabel, handleFormSubmit} = this.props;
    return(
      <form className="customer-input-form" onSubmit={()=> handleFormSubmit(this.state)}>
        <TextInput 
          name="customer-first-name" 
          label="First Name:"
          placeholder="Enter First Name"
          onInputType={this.onFirstNameInputType}
          value={firstName}
        />
        <TextInput 
          name="customer-last-name" 
          label="Last Name:"
          placeholder="Enter Last Name"
          onInputType={this.onLastNameInputType}
          value={lastName}
        />
        <p>Date of Birth:</p>
        <DatePicker 
          onChange={this.onDateChange}
          selected={dob}
        />
        <input 
          className="btn submit" 
          type="submit" 
          value={inputBtnLabel} 
          style={addBtnStyle}  
        />
      </form>
    );
  }
};

const addBtnStyle = {
  'display': 'block',
  'margin': '20px 0 10px 0',
  'padding': '10px 30px',
  'backgroundColor': '#45a7e0',
  'color': '#fff'
};

Form.propTypes = {
  inputBtnLabel: PropTypes.string,
  handleFormSubmit: PropTypes.func
};