import React from "react";
import { shallow, mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import TextInput from "./";

configure({ adapter: new Adapter() });

describe("<TextInput />", () => {
  const props = {
    label: "First Name",
    name: "firstName",
    value: "Mike",
    placeholder: "Enter first Name",
    onInputType: () => {}
  };
  
  const textInput = shallow(<TextInput  {...props}/>);

  describe("has default behavior", () => {
    it("renders correctly", () => {
      expect(textInput).toBeDefined();
      expect(typeof textInput).toBe("object");
      expect(textInput.find(".input-text").exists()).toEqual(true);
    });
  });

  describe("when all props are missing", () => {
    it("fails to render", () => {
      expect(() => textInput.toThrow());
    });
  });
});