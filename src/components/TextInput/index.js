// This re-usable functional styled component is responsible for accepting text inputs
import React from 'react';
import PropTypes from 'prop-types';

const TextInput = (props) => {
  const {name, value, placeholder, onInputType, label} = props;
  return (
    <label> {label}
      <input type="text"
        className="input-text"
        name={name} 
        value={value}
        placeholder={placeholder}
        onChange={(e) => onInputType(e)} 
        required
        style={inputStyle}  
      />
    </label>   
  )
};

const inputStyle = {
  'borderRadius': '5px',
  'display': 'block',
  'width': '100%'
};

TextInput.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  name: PropTypes.string,
  placeholder: PropTypes.string,
  onInputType: PropTypes.func
};

export default TextInput;