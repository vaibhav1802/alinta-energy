// This component is responsible for displaying the list of customers passed to it as a prop
import React from 'react';
import PropTypes from 'prop-types';
import getMonth from '../../utilities/getMonth';
import actions from '../../state-management/actions';

const CustomerListDisplay = (props) => {
  const {customerList} = props;

  function onDeleteCustomer(customerId) {
    actions.deleteCustomer(customerId);
  }

  return(
    <ul>
    {
      customerList.map((customer) => 
        <li className="list-item" id={customer.customerId} key={customer.customerId}>
          <div className="customer-info">
            <div>
              <span className="name-label">Name:</span>
              <span className="first-name">{customer.firstName}</span>
              <span className="last-name">{customer.lastName}</span>
            </div>
            <div>
              <span className="dob-label">DOB:</span>
              <span className="dob-date">{customer.dob.getDate()}</span>
              <span className="dob-month">&nbsp;-&nbsp;{getMonth(customer.dob.getMonth())}&nbsp;</span>
              <span className="dob-year">-&nbsp;{customer.dob.getFullYear()}</span>
            </div>
            <a href={`#/edit/${customer.customerId}`} className="btn edit">Edit</a>
            <button onClick={() => onDeleteCustomer(customer.customerId)} className="btn delete">Delete</button>
          </div>
        </li>
      )
    }
    </ul>
  )
};

CustomerListDisplay.propTypes = {
  customerList: PropTypes.array
};

export default CustomerListDisplay;