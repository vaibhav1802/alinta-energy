import getMonth from './getMonth';

describe('Test for showing month name', () => {
  
  test('Check for the first month', () => {
    expect(getMonth(0)).toEqual('January');
  });
  test('Check for the last month', () => {
    expect(getMonth(11)).toEqual('December');
  });
  test('Check for invalid month', () => {
    expect(getMonth(13)).toEqual(undefined);
  });
});